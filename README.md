# Sinatra Template

## Summary

Simple Sinatra framework with back-end and front-end utilities. Also, it tries to follow the [KISS principle](https://en.wikipedia.org/wiki/KISS_principle).

Live preview can be found [here](http://dhc-sinatra-template.herokuapp.com/).

## Installation

Clone this repo, install the dependencies with

````sh
bundle install
````

And execute the server with

````sh
bundle exec rackup
````

You can run an interactive console with

````sh
racksh
````

Also, this is a template, so do whatever you want it.

## Folder structure

````
sinatra-template  # Root folder
|- Gemfile        # Dependencies
|- main.rb        # Main program
|- public         # Public content loaded by sinatra. Files and folders go here, accessed via '<folders>/<file.extension>' url.
|                 # Also, this folder includes precompiled assets.
|- config         # Application initializers
|   |- locales     # Localization files folder
|
|- assets         # Assets folder. See sinatra-assetpack
|   |- js          # Javascript folder. Accessed as '/js/<file>.js' url
|   |- css         # Stylesheets folder. Accessed as '/css/<file>.css' url
|   |- fonts       # Fonts folder. Accessed as '/fonts/<file>.<ext>' url
|   |- images      # Images folder. Accessed as '/images/<file>.<ext>' url
|
|- models         # Database objects; one per file
|- routes         # Sinatra routes file; one per routes set (as you wish)
|- views          # Sinatra views (slim) files
   |- layout.slim   # Layout file
   |- index.slim    # Home page
````

Please, note that files ubicated in `config`, `models` and `routes` will be loaded automatically.

## Debug

Use [`pry`](https://github.com/pry/pry) to debug your code. Just place a `binding.pry` anywhere in your code to create a breakpoint.

## Enviroment variables

| Key | Description | Default |
| --- | ----------- | ------- |
| ADMIN_USER | Authentication user for `protected!` pages | admin |
| ADMIN_PASSWORD | Authentication password for `protected!` pages | admin |

## TODO

- Create a separated example (WebSockets chat, for example).
- Add more information to README.md (dependencies, examples, ...).
- Implement a production server (like Puma or thin).
- Add support with a deployment tool (like capistrano).
- Add a wiki with tutorials and available features (like WebSockets).

## Authors

This project has been developed by:

| Avatar | Name | Nickname | Email |
| ------- | ------------- | --------- | ------------------ |
| ![](http://www.gravatar.com/avatar/2ae6d81e0605177ba9e17b19f54e6b6c.jpg?s=64)  | Daniel Herzog | Wikiti | [wikiti.doghound@gmail.com](mailto:wikiti.doghound@gmail.com) |
