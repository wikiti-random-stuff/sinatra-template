$(document).ready ->
  chat = $ '#chat'
  form = $ 'form'
  input = $ 'input.send'
  name = $ 'input.name'
  ws = new WebSocket('ws://' + location.host + location.pathname)

  append = (msg, json) ->
    msg = "<strong>#{parse(msg).name}</strong>: #{parse(msg).msg}" if json == true
    chat.html "#{msg}<br/>#{chat.html()}"

  parse = (msg) ->
    JSON.parse msg

  toMessage = (obj) ->
    JSON.stringify obj

  ws.onopen = ->
    append 'websocket opened'

  ws.onclose = ->
    append 'websocket closed'

  ws.onmessage = (m) ->
    append m.data, true

  input.click ->
    input.val ''

  form.submit ->
    ws.send toMessage
      name: name.val()
      msg: input.val()

    input.val ''
    false
