resource :examples do
  get { 'index' }
  post { 'create' }

  member do
    get { |id| 'show' }
    delete { |id| 'delete' }
    patch { |id| 'patch' }

    get('custom') { |id| 'custom' }
  end
end
