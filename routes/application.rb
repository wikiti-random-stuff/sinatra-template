# Routes
get '/' do
  slim :index
end

resource :locales do
  get :set do
    session[:locale] = params[:locale].to_sym unless params[:locale].nil?
    redirect back
  end
end

before do
  session[:locale] = I18n.default_locale unless I18n.available_locales.include? session[:locale]
  I18n.locale = session[:locale]
end
