helpers do
  def protected!
    return if authorized?
    headers['WWW-Authenticate'] = 'Basic realm="Restricted Area"'
    halt 401, "Not authorized\n"
  end

  def authorized?
    @auth ||=  Rack::Auth::Basic::Request.new(request.env)
    @auth.provided? && @auth.basic? && @auth.credentials && @auth.credentials == credentials
  end

  def js_include(*files)
    files.map { |f| "<script type='text/javascript' src='/js/#{f}.js'></script>" }.join
  end

  def css_include(*files)
    files.map { |f| "<link rel='stylesheet' href='/css/#{f}.css'>" }.join
  end

  def t(*keys, **opts)
    translate *keys, **opts
  end

  def translate(*keys, **opts)
    I18n.t *keys, **opts
  end

  private

  def credentials
    @credentials ||= [ENV['ADMIN_USER'] || 'admin', ENV['ADMIN_PASSWORD'] || 'admin']
  end
end
