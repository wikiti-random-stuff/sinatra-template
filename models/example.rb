class Example
  include DataMapper::Resource

  property :id, Serial
  property :name, String
  property :points, Integer
end
